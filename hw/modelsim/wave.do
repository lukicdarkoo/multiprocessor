onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /testbench/Clk
add wave -noupdate /testbench/nReset
add wave -noupdate /testbench/Address
add wave -noupdate /testbench/ChipSelect
add wave -noupdate /testbench/Read
add wave -noupdate /testbench/Write
add wave -noupdate -radix hexadecimal /testbench/ReadData
add wave -noupdate -radix hexadecimal /testbench/WriteData
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1616799 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 172
configure wave -valuecolwidth 74
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {17952768 ps}
