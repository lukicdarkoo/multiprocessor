-- Formatted using: https://chan.mit.edu/vhdl-formatter

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Counter is
	port (
		Clk : in std_logic;
		nReset : in std_logic;
		Address : in std_logic_vector(2 downto 0);
		ChipSelect : in std_logic;
		Read : in std_logic;
		Write : in std_logic;
		ReadData : out std_logic_vector(31 downto 0);
		WriteData : in std_logic_vector(31 downto 0)
	);
end Counter;

architecture comp of Counter is
	signal iCounter : unsigned(31 downto 0);
	signal incVal : unsigned(31 downto 0);
 
begin
	pRegWr : process(Clk, nReset) begin
		if nReset = '0' then
			incVal <= (others => '0');
			iCounter <= (others => '0');
		elsif rising_edge(Clk) then
			if ChipSelect = '1' and Write = '1' then -- Write cycle
				case Address(2 downto 0) is
					when "010" => iCounter <= unsigned(WriteData);
					when "011" => incVal <= unsigned(WriteData);
					when "100" => iCounter <= iCounter + incVal;
					when "101" => iCounter <= iCounter - incVal;
					when others => null;
				end case;
			end if;
		end if;
	end process pRegWr;
 
 
	pRegRd : process(Clk) begin
		if rising_edge(Clk) then
			ReadData <= (others => '0');
			if ChipSelect = '1' and Read = '1' then -- Read cycle
				case Address(2 downto 0) is
					when "000" => ReadData <= std_logic_vector(iCounter);
					when "001" => ReadData <= std_logic_vector(incVal);
					when others => null;
				end case;
			end if;
		end if;
	end process pRegRd;
 
end comp;