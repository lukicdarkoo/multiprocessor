#include <stdio.h>
#include <string.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_uart.h"
#include "altera_avalon_mailbox_simple.h"
#include "altera_avalon_mailbox_simple_regs.h"
#include "altera_avalon_mutex.h"


#define SHARED_MSG_ADDR 0x0000
char *shared_msg = SHARED_MSG_ADDR;


static void mailbox_tx(void *message, int status) { }

void test_mailbox() {
	int counter = 0;

	altera_avalon_mailbox_dev* send_dev;
	send_dev = altera_avalon_mailbox_open("/dev/mailbox_simple_0", mailbox_tx, NULL);

	while (1) {
		alt_u32 message = counter;
		sprintf(shared_msg, "Message from CPU1 #%d\n", counter++);
		altera_avalon_mailbox_send(send_dev, &message, 0, POLL);

		for (int i = 0; i < 1000000; i++);
	}
}

void test_mutex() {
	alt_mutex_dev* mutex = altera_avalon_mutex_open("/dev/mutex_0");
	if (! mutex) {
		printf("Mutex error!\n");
	}

	while (1) {
		altera_avalon_mutex_lock(mutex, 1);
		int counter = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);

		counter--;

		IOWR_ALTERA_AVALON_PIO_SET_BITS(PIO_0_BASE, counter);
		IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(PIO_0_BASE, ~counter);
		altera_avalon_mutex_unlock(mutex);

		for (int i = 0; i < 100000; i++);
	}
}

int main(void) {
	test_mutex();

	return 0;
}
