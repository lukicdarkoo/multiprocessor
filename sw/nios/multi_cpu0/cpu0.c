#include <stdio.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_mailbox_simple.h"
#include "altera_avalon_mailbox_simple_regs.h"
#include "altera_avalon_mutex.h"
#include "altera_avalon_performance_counter.h"

#define COUNTER_1_BASE 0x4041040


#define SHARED_MSG_ADDR 0x0000
char *shared_msg = SHARED_MSG_ADDR;


// Performance Timer shortcuts
#define TIC PERF_RESET(PERFORMANCE_COUNTER_0_BASE); \
	PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE); \
	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 1);
#define TOC PERF_END(PERFORMANCE_COUNTER_0_BASE, 1); \
	PERF_STOP_MEASURING(PERFORMANCE_COUNTER_0_BASE);
int perf_time() {
	return perf_get_total_time(PERFORMANCE_COUNTER_0_BASE);
}

void increment_pio() {
	int counter = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
	counter++;
	IOWR_ALTERA_AVALON_PIO_SET_BITS(PIO_0_BASE, counter);
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(PIO_0_BASE, ~counter);
}

static void mailbox_rx(void *message, int status) {
	printf("Message from mailbox with status %d and message %d \n", status, *((alt_u32 *)message));
}

void test_mailbox() {
	altera_avalon_mailbox_dev* recv_dev;
	recv_dev = altera_avalon_mailbox_open("/dev/mailbox_simple_0", NULL, mailbox_rx);
	if (! recv_dev) {
		printf("Mailbox error!\n");
	}
}

void test_mutex() {
	alt_mutex_dev* mutex = altera_avalon_mutex_open("/dev/mutex_0");
	if (! mutex) {
		printf("Mutex error!\n");
	}
	int perf = 0;


	while (1) {
		TIC
		altera_avalon_mutex_lock(mutex, 1);
		increment_pio();
		altera_avalon_mutex_unlock(mutex);
		TOC
		perf = perf_time();

		TIC
		increment_pio();
		TOC

		printf("Mutex cycles taken %d, without %d\n", perf, perf_time());


		for (int i = 0; i < 200000; i++);
	}
}

void test_counter() {
	// IOWR(COUNTER_0_BASE, 2, 0);
	IOWR(COUNTER_0_BASE, 3, 1);

	while (1) {

		int value = IORD(COUNTER_0_BASE, 0);
		IOWR(COUNTER_0_BASE, 4, 0);

		printf("%u\n", value);

		for (int i = 0; i < 200000; i++);
	}
}

int main(void) {
	IOWR_ALTERA_AVALON_PIO_SET_BITS(PIO_0_BASE, 3);
	printf("Hello world from CPU0\n");
	// printf(shared_msg);




	test_counter();
	//test_mutex();

	return 0;
}


